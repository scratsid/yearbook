% rebase('base.tpl', title='Profile Page')
% user.pop('hash')
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
        <h1 class="header center orange-text">{{user['Name']}}</h1>
        % if user.get('nick'):
            <h5 class="header center orange-text">aka {{user['nick']}}</h5>
        % end

        % from time import time
        % imgtime = time()
        % if now_photo:
            <div class="row center">
                <div class="col s12 m5">
                    <img src="/static/then/photos/{{user['Roll']}}.webp?{{imgtime}}" class="col s12">
                    <h6 class="light"> Then.. <a href="#" class="small right material-icons" id="edit_then">create</a></h6>
                    <div id="then" style="display: none;">
                        % include('photoupload', photo_type="then", default=dp_dir.get(user['Roll'])=='then')
                    </div>
                    <br>
                </div>
                <div class="col s12 m2"></div>
                <div class="col s12 m5">
                    <img src="/static/now/photos/{{user['Roll']}}.webp?{{imgtime}}" class="col s12">
                    <h6 class="light"> ..Now <a href="#" class="small right material-icons" id="edit_now">create</a></h6>
                    <div id="now" style="display: none;">
                        % include('photoupload', photo_type="now", default=dp_dir.get(user['Roll'])=='now')
                    </div>
                    <br>
                </div>
            </div>
        % else:
            <div class="row center">
                <h5 class="header col s12 m3 light"></h5>
                <h5 class="header col s12 m6 light"><img src="/static/then/photos/{{user['Roll']}}.webp" class="col s12 center"></h5>
            </div>
            <div class="row center">
                <div>
                    <h6>Please upload a current photo</h6>
                    % include('photoupload', photo_type="now", default=True)
                </div>
            </div>

        % end

        <div id="details">
            % include('userdetails.tpl')
            <div class="row center">
                <button id="edit_details" class="btn waves-effect waves-light orange">Edit Profile</button>
            </div>
        </div>
        <form id="details_form" style="display: none;" method="post">
        <div class="row center">
            <div class="input-field col s12 m6">
                <input value="{{user.get('nick','')}}" id="nick" name="nick" type="text" class="validate">
                <label for="nick">Nickname</label>
            </div>
            <div class="input-field col s12 m6">
                <input value="{{user.get('location','')}}" id="location" name="location" type="text" class="validate">
                <label for="location">Location (City, State, Country)</label>
            </div>
            <div class="input-field col s12">
                <textarea id="aboutme" name="aboutme" class="materialize-textarea">{{user.get('aboutme','')}}</textarea>
                <label for="aboutme">About Me</label>
            </div>
            <div class="input-field col s12 m6">
              <input value="{{user.get('email','')}}" id="email" name="email" type="email" class="validate">
              <label for="email">Email</label>
            </div>
            <div class="input-field col s12 m6">
                <input value="{{user.get('mobile','')}}" id="mobile" name="mobile" type="text" class="validate">
                <label for="mobile">Mobile No</label>
            </div>
            <div class="input-field col s12 m6">
                <input value="{{user.get('hobbies','')}}" id="hobbies" name="hobbies" type="text" class="validate">
                <label for="hobbies">Hobbies & Interests</label>
            </div>

            % for field in ['facebook', 'linkedin', 'instagram', 'youtube', 'website']:
                <div class="input-field col s12 m6">
                    <input value="{{user.get(field,'')}}" id="{{field}}" name="{{field}}" type="text" class="validate">
                    <label for="{{field}}">{{field}}</label>
                </div>
            % end
        </div>
        <div class="row center">
            <button class="btn waves-effect waves-light orange" type="submit">Update
                <i class="material-icons right">send</i>
            </button>
        </div>
        </form>
      <br><br>
    </div>
</div>

<div class="container">
    <div class="section">
      <!--   Icon Section   -->
        % include('testimonials', by='Sender')
    </div>
    <br><br>
</div>
