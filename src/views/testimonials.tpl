<div class="row">
  % for testimonial in testimonials:
    <div class="col s12 m6">
      <div class="icon-block">
        <div class="center"><a href="/user/{{testimonial[by]['Roll']}}">
            <img width="25%" class="circle" src="/static/{{dp_dir.get(testimonial[by]['Roll'], 'then')}}/thumbs/{{testimonial[by]['Roll']}}.webp?">
        </a></div>
        <a href="/user/{{testimonial[by]['Roll']}}"><h5 class="center">{{testimonial[by]['Name']}}</h5></a>

        <h6 style="white-space: pre-wrap;" class="light">{{testimonial['Testimonial']}}</h6>
      </div>
    </div>
  % end
</div>
