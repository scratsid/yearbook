% rebase('base.tpl', title='Admin Page')
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
        <h1 class="header center orange-text">Admin Page</h1>
        <div class="row center">
            <form action="/admin" method="post" enctype="multipart/form-data">
                Select a file: <input type="file" name="upload" /><br><br>
                <input class="btn orange" type="submit" value="Upload csv file" />
            </form>
        </div>
      <br><br>
    </div>
</div>
