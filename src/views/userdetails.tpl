<div class="row">
    % for field, icon in {'aboutme': 'person', 'location':'person_pin_circle', 'mobile': 'phone_android', 'hobbies': 'golf_course'}.items():
    <div class="col s12 m6">
        <h6 style="white-space: pre-wrap;"><i class="small material-icons">{{icon}}</i> {{user.get(field,'')}}</h6>
    </div>
    % end
</div>
<div class="row">
    <div class="col s12 m6">
            <h6><a href="mailto:{{user.get('email','')}}" class="fa fa-envelope">: {{user.get('email','')}}</a></h6>
    </div>
    <div class="col s12 m6">
            <h6><a href="https://{{user.get('website','')}}" target="_blank" class="fa fa-chrome">: {{user.get('website','')}}</a></h6>
    </div>
    % for field in ['facebook', 'linkedin', 'instagram', 'youtube']:
        <div class="col s12 m6">
            <h6><a href="{{user.get(field,'')}}" target="_blank" class="fa fa-{{field}}">: {{user.get(field,'')}}</a></h6>
        </div>
    % end

</div>
