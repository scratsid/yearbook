% rebase('base.tpl', title='Profile Page')
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <div class="row center">
            <h6 class="header col s12 m6" style="color: orange">Testimonials received: {{testimonials_received}}</h6>
            <h6 class="header col s12 m6" style="color: orange">Photos uploaded: {{len(dp_dir)-1}}</h6>
      </div>
      <form id="search-form">
        <div class="row">
            <div class="input-field col s7 m9 l10">
              <input id="search" type="text" class="validate">
              <label for="search">Search</label>
            </div>
            <div class="input-field col s5 m3 l2">
                <button class="btn waves-effect waves-light orange" type="submit" id="search-btn" name="search-btn">Search
                    <i class="material-icons right">search</i>
                </button>
            </div>
        </div>
      </form>
      <br><br>
    </div>
</div>
<div class="container">
    <div class="section">
      <!--   Icon Section   -->
      <div class="row">
        % for user in users:
        <div class="col s12 m4">
          <div class="card small">
            <div class="center"><a href="/user/{{user['Roll']}}"><img width="50%" class="circle" src="/static/{{dp_dir.get(user['Roll'], 'then')}}/thumbs/{{user['Roll']}}.webp"></a></div>
            <p class="center">{{user['Roll']}}</p>
            <a href="/user/{{user['Roll']}}"><h5 name="Name" id="Name" class="center name">{{user['Name']}}</h6></a>
          </div>
        </div>
        % end
      </div>
    </div>
</div>
