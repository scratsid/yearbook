% rebase('base.tpl', title='Yearbook')
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
        <h1 class="header center orange-text">Welcome to PGP09-Yearbook</h1>
        <div class="row center">
            <h5 class="header col s12 light">Connect once more to your old partners in crime and spill their secrets in the tesmonials</h5>
        </div>
        <div class="row center">
            <a href="/users" id="download-button" class="btn-large waves-effect waves-light orange">Get Started</a>
        </div>
      <br><br>
    </div>
</div>

 <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">cloud_upload</i></h2>
            <h5 class="center">How To Log in</h5>

            <p>Your username is simply your Roll No. in the format PGP09-xxx (eg PGP09-020). Your password is the longest part of your name. Ex: Karayanan Krinivasan K. The Password would be "Krinivasan" and yes the passwords are case-sensitive. In case your name has equal longest parts then you need to try out all of those as we ourselves do not know which one the computer would have chosen. </p>
            <p>In case you do not remember your Roll No call Anchit or B.Rajesh however if you don't remember your name contact us with your Roll No and we'll try to find your name</p>
            <p class="light">Why is it so? Primarily because we did not have verified emails/mobile nos for all our batchmates to send you OTP/Code for login, so thats what you get for not keeping your information up to date with Alcom. Secondly because we thought it'd be fun to watch you sweat it out.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">search</i></h2>
            <h5 class="center">Searching for Batchmates</h5>

            <p>When searching for batchmates kindly note the search is case sensitive too. So searching for "abhi" would fetch nothing while "Abhi" will get you quite a few results. Since partial searches are also possible searching for "kar" might turn up some unexpected names. </p>
            <p class="light">Why is it so? Primarily because use of capital letters denotes the respect your successful colleagues deserve. Secondly because it was easier to code. Had we implemented a case insesitive search we would not have the time to write all this crap.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">directions_boat</i></h2>
            <h5 class="center">Where are we headed</h5>

            <p>We're still working on this and need to implement the following:</p>
            <ol>
                <li>Printer Friendly PDF export for the yearbook</li>
            </ol>
            <p>However since K is such a democratic, we let you decide what other features you desire from this Yearbook. To sweeten the deal further not only do you get to decide the features you get to implement them too. We look forward for your patches and commits. You can find the code at the repo url: https://gitlab.com/scratsid/yearbook</p>
            <p class="light">Why is it so? Because thats what Student Council instilled in us. So 'take the initiative' and 'Do it for K'</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">gavel</i></h2>
            <h5 class="center">How brutal can I be with this system</h5>

            <h6><u>Not At All</u></h6>

            <p>The system is barely held together by some 200 lines of duct tape code and almost zero testing. If you try to break it, you will succeed. Even if you're not trying to break it, you will succeed. So be gentle and report the bugs you find, which we might fix or not.</p>

            <p class="light">Why is it so? Because currently we do not have any experienced programmer on the team, neither do we have the support of IT Comm. Unless one of the Big Shots steps in, we will continue churning out crappy code. If you do not like the current state of things you can claim ownership of the project and we will gladly step into an assisting role</p>
          </div>
        </div>
      </div>

    </div>
    <br><br>
  </div>
