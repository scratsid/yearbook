<form method="post" action="/user/photo" enctype="multipart/form-data">
    <div class = "file-field input-field">
      <div class = "btn orange">
         <span>Browse</span><input name="photo" type = "file" />
      </div>
      <div class = "file-path-wrapper">
         <input class = "file-path validate" type = "text" placeholder = "Upload file" />
      </div>
      <div class='light'><label>
        <input type="checkbox" name="default" {{default and 'checked="checked"'}} />
        <span>Set as DP</span>
      </label></div>
    </div>
    <button class="btn waves-effect waves-light orange" type="submit" name="{{photo_type}}">Upload Photo
        <i class="material-icons right">cloud_upload</i>
    </button>
</form>
