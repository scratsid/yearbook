% rebase('base.tpl', title='Yearbook')
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
        <h3 class="header center orange-text">Change Password</h3>
        <form method="post">
        <div class="row center">
            <input id="oldpass" name="oldpass" type="password" class="validate">
            <label for="oldpass">Old Password</label>
            <input id="oldpass" name="newpass" type="password" class="validate">
            <label for="oldpass">New Password</label>
            <input id="oldpass" name="reppass" type="password" class="validate">
            <label for="oldpass">Confirm Password</label>
        </div>
        <div class="row center">
            <button class="btn waves-effect waves-light orange" type="submit" name="action">Change Password
                    <i class="material-icons right">send</i>
            </button>
        </div>
        </form>
      <br><br>
    </div>
</div>
