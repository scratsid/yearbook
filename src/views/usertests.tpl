% rebase('base.tpl', title='Yearbook')
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
        <h2 class="header center orange-text">Testimonials Written By Me</h2>
      <br><br>
    </div>
</div>
<div class="container">
    <div class="section">
      <!--   Icon Section   -->
        % include('testimonials', by='Receiver')
    </div>
    <br><br>
</div>
