% rebase('base.tpl', title='Profile Page')
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
        <h1 class="header center orange-text">{{user['Name']}}</h1>
        % if user.get('nick'):
            <h5 class="header center orange-text">aka {{user['nick']}}</h5>
        % end

        % from time import time
        % imgtime = time()
        % if now_photo:
            <div class="row center">
                <div class="col s12 m5">
                    <img src="/static/then/photos/{{user['Roll']}}.webp?{{imgtime}}" class="col s12">
                    <h6 class="light"> Then.. </h6>
                    <h6></h6>
                </div>
                <div class="col s12 m2"></div>
                <div class="col s12 m5">
                    <img src="/static/now/photos/{{user['Roll']}}.webp?{{imgtime}}" class="col s12">
                    <h6 class="light"> ..Now </h6>
                    <h6></h6>
                </div>
            </div>
        % else:
            <div class="row center">
                <h5 class="header col s12 m3 light"></h5>
                <h5 class="header col s12 m6 light"><img src="/static/then/photos/{{user['Roll']}}.webp" class="col s12 center"></h5>
            </div>
        % end

        % include('userdetails.tpl')
        <form method="post">
            <div class="row center">
                <div class="input-field col s12">
                    % testimonial = next((t['Testimonial'] for t in testimonials if t['Sender']['Roll']==logged['Roll']), '')
                    <textarea id="testimonial" name="testimonial" class="materialize-textarea">{{testimonial}}</textarea>
                    <label for="testimonial">Testimonial</label>
                </div>
            </div>
            <div class="row center">
                <button class="btn waves-effect waves-light orange" type="submit" name="action">Submit
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </form>
      <br><br>
    </div>
</div>
<div class="container">
    <div class="section">
        <!--   Icon Section   -->
        % include('testimonials', by='Sender')
    </div>
    <br><br>
</div>
