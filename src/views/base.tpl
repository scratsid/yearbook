<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>{{title or 'YearBook'}}</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="/static/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="/static/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <!--font-awesome.min.css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Yearbook</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="/users">Batchmates</a></li>
        <li><a href="/user">My Profile</a></li>
        <li><a href="/user/testimonials">Testimonials by Me</a></li>
        <li><a href="/changepass">Change Password</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="/users">Batchmates</a></li>
        <li><a href="/user">My Profile</a></li>
        <li><a href="/user/testimonials">Testimonials by Me</a></li>
        <li><a href="/changepass">Change Password</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  % if get('msg'):
    <div class="row center"><h5 style="color: red">{{msg}}</h5></div>
  % end
  {{!base}}

  <footer class="page-footer orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">YearBook</h5>
          <h6 class="grey-text text-lighten-4">An attempt to capture our fleeting youth and carefree days.</h6>
          <h6 class="grey-text text-lighten-4">Reminder: Nostalgia (Nov 4-6, 2022) is an opportunity to catch up with old friends and form alliances to become Business Legends. Register early for the best living arrangements.</h6>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Settings</h5>
          <ul>
            <li><a class="white-text" href="/changepass"><h6>Change Password</h6></a></li>
            <li><a class="white-text" href="/user"><h6>My Profile</h6></a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Konnect</h5>
          <ul>
            <li><a class="white-text" href="/users"><h6>Batchmates</h6></a></li>
            <li><a class="white-text" href="/user/testimonials"><h6>Testimonials by Me</h6></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Theme by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/static/js/materialize.min.js"></script>
  <script src="/static/js/init.js"></script>
  <script>

    $('#edit_then').click(function(){
        $('#then').show()
        event.preventDefault();
    });

    $('#edit_now').click(function(){
        $('#now').show()
        event.preventDefault();
    });

    $('#edit_details').click(function(){
        $('#details').hide();
        $('#details_form').show();
    });

    $('#search-form').submit(function(){
        var name = $('#search').val()
        $('.name').parent().parent().parent().hide();
        $(`.name:contains(${name})`).parent().parent().parent().show()
        event.preventDefault();
    });

  </script>
  </body>
</html>
