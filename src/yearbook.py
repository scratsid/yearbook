#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# A very simple bottle graduation yearbook
from bottle import default_app, route, auth_basic, request, run, template, redirect, post, static_file
from tinydb import TinyDB, Query
from hashlib import sha224
from img_pad import img_pad
from pathlib import Path
import csv

db = TinyDB('db/db.json', ensure_ascii=False)
Q = Query()
users = db.table('users')
tests = db.table('testimonials')


def is_authenticated_user(user, password):
    ''' User Authentication '''
    # calculate password hash #
    passhash = sha224(bytes(password, 'utf-8')).hexdigest()

    # check if user & password in user table or if admin with default hash #
    if users.get((Q.Roll == user) & (Q.hash == passhash)) or \
       (user == 'admin' and db.get(Q.hash == passhash)):
        return True
    else:
        return False


@route('/')
def home():
    ''' landing page '''
    return template('index')


@route('/users')
@auth_basic(is_authenticated_user)
def list():
    ''' list all users '''
    if request.auth[0] == 'admin':
        redirect('/admin')
    return template('list', users=users.all(), dp_dir=db.get(Q.dp_dir.exists()),
                    testimonials_received=len(tests))


@route('/user')
@auth_basic(is_authenticated_user)
def profile():
    ''' Show profile page of current user '''
    if request.auth[0] == 'admin':
        redirect('/admin')
    user = users.get(Q.Roll == request.auth[0])
    return template('profile', user=user, dp_dir=db.get(Q.dp_dir.exists()),
                    testimonials=tests.search(Q.Receiver.Roll == user['Roll']),
                    now_photo=Path(f"static/now/photos/{user['Roll']}.webp").is_file())


@post('/user')
@auth_basic(is_authenticated_user)
def user_update():
    ''' update user profile'''
    users.update(request.forms.decode('utf-8'), Q.Roll == request.auth[0])
    redirect('/user')


@route('/user/testimonials')
@auth_basic(is_authenticated_user)
def testimonials():
    ''' Show profile page of current user '''
    if request.auth[0] == 'admin':
        redirect('/admin')
    return template('usertests', user=users.get(Q.Roll == request.auth[0]),
                    dp_dir=db.get(Q.dp_dir.exists()),
                    testimonials=tests.search(Q.Sender.Roll == request.auth[0]))


@route('/user/<roll>')
@auth_basic(is_authenticated_user)
def userpage(roll):
    ''' show user page with editable testimonial field '''
    if roll == request.auth[0]:
        redirect('/user')
    user = users.get(Q.Roll == roll)
    if user:
        return template('userpage', user=user, dp_dir=db.get(Q.dp_dir.exists()),
                        logged=users.get(Q.Roll == request.auth[0]),
                        testimonials=tests.search(Q.Receiver.Roll == user['Roll']),
                        now_photo=Path(f"static/now/photos/{user['Roll']}.webp").is_file())


@post('/user/photo')
@auth_basic(is_authenticated_user)
def photo():
    ''' update user photo'''

    # If image is set as default update database #
    folder = 'now' if 'now' in request.forms else 'then'
    if request.forms.default:
        db.upsert({request.auth[0]: folder}, Q.dp_dir.exists())

    # if no image file is uploaded redirect to profile #
    photo_file = request.files.get('photo')
    if not photo_file or photo_file.filename == "empty":
        redirect('/user')

    # check and save the uploaded file #
    if photo_file.content_type in ['image/jpg', 'image/png', 'image/jpeg']:
        src_file = f'uploads/{photo_file.filename}'
        photo_file.save(src_file, True)
        try:
            img_pad(src_file, f'static/{folder}/photos/{request.auth[0]}.webp', size=(720, 720))
            img_pad(src_file, f'static/{folder}/thumbs/{request.auth[0]}.webp', size=(240, 240))
        except Exception:
            msg = "Error identifying image type, kindly check image file and try again"
        else:
            Path(src_file).unlink(True)
            redirect('/user')
    else:
        msg = "Kindly ensure the image file is a jpg or png image and try again"

    user = users.get(Q.Roll == request.auth[0])
    return template('profile', user=user, dp_dir=db.get(Q.dp_dir.exists()), msg=msg,
                    testimonials=tests.search(Q.Receiver.Roll == user['Roll']),
                    now_photo=Path(f"static/now/photos/{user['Roll']}.webp").is_file())


@post('/user/<roll>')
@auth_basic(is_authenticated_user)
def testimonial(roll):
    ''' post/edit testimonial for user with passed roll '''
    user = users.get(Q.Roll == roll)
    logged = users.get(Q.Roll == request.auth[0])
    if user:
        tests.upsert({'Receiver': {'Roll': user['Roll'], 'Name': user['Name']},
                      'Sender': {'Roll': logged['Roll'], 'Name': logged['Name']},
                      'Testimonial': request.forms.testimonial},
                     (Q.Receiver.Roll == user['Roll']) & (Q.Sender.Roll == logged['Roll']))
    redirect('/user/'+roll)


@route('/testimonial/approval/<id>')
@auth_basic(is_authenticated_user)
def approval(roll):
    ''' Toggle the approval status of a testimonial written for the user '''
    pass


@route('/changepass')
@auth_basic(is_authenticated_user)
def change_pass():
    ''' change password '''
    return template('changepass')


@post('/changepass')
@auth_basic(is_authenticated_user)
def update_pass():
    ''' change password '''
    # convert password to hashes #
    oldhash = sha224(bytes(request.forms.get('oldpass'), 'utf-8')).hexdigest()
    newhash = sha224(bytes(request.forms.get('newpass'), 'utf-8')).hexdigest()

    if request.forms.get('newpass') == request.forms.get('reppass'):
        if request.auth[0] == 'admin':
            status = db.update({'hash': newhash}, (Q.hash == oldhash))
        else:
            status = users.update({'hash': newhash}, (Q.Roll == request.auth[0]) & (Q.hash == oldhash))

        # if update is successful #
        if status:
            return template('index', msg='Password changed. Please log in with your new password')

    return template('changepass', msg='Password change failed miserably. Please keep trying')


@route('/admin')
@auth_basic(is_authenticated_user)
def admin():
    ''' Admin Page for app configuration '''

    if request.auth[0] != 'admin':
        redirect('/user')
    return template('admin')


@post('/admin')
@auth_basic(is_authenticated_user)
def csv_load():
    ''' Load user details from csv file '''

    # check and save the uploaded file #
    csv_file = request.files.get('upload')
    if not(csv_file and csv_file.content_type == 'text/csv'):
        return template('admin', msg='Improper File Format')

    csv_file.save('uploads/users.csv', True)

    # read user details from csv file and update the db #
    # Its required to have at least two fields in the csv (Roll, Name) #
    with open('uploads/users.csv') as csvfile:
        user_list = csv.DictReader(csvfile)
        for user in user_list:
            # if a password field is missing the longest name fragment is set as password #
            if not user.get('password'):
                user['password'] = sorted(user['Name'].split(), key=len)[-1]
            # generate a hash for password and remove (pop) the password field from csv dict #
            user['hash'] = sha224(bytes(user.pop('password'), 'utf-8')).hexdigest()
            # add to database #
            users.upsert(user, Q.Roll == user['Roll'])

    return template('admin', msg='Uploaded the csv contents to DB')


@route('/admin/fixthumbs')
@auth_basic(is_authenticated_user)
def fixthumbs():
    ''' Reset Thumbs folder to then for users who did not upload the now image but set the now image as dp'''

    dp_dir = db.get(Q.dp_dir.exists())
    for roll, folder in dp_dir.items():
        dp_dir[roll] = folder if Path(f'static/{folder}/thumbs/{roll}.webp').is_file() else 'then'
    db.update(dp_dir, doc_ids=[dp_dir.doc_id])
    return ("Reset thumbnails")


@route('/static/<filepath:path>')
def send_static(filepath):
    return static_file(filepath, root='static')


application = default_app()


if __name__ == '__main__':
    from bottle import TEMPLATE_PATH
    TEMPLATE_PATH.insert(0, './src/views')
    run(application, host='0.0.0.0', port=8080, reloader=True, debug=True)
