FROM python:slim
MAINTAINER Scrat
ADD requirements.txt /
RUN pip install -r requirements.txt
RUN mkdir yearbook
ADD . yearbook/
WORKDIR yearbook
RUN cp db/sample.db.json db/db.json
EXPOSE 8080
ENTRYPOINT ["python3","src/yearbook.py"]
