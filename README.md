Yearbook is a simple web app for deploying a graduation yearbook for the outgoing class where friends can drop in testimonials for you.

The current structure of the repo allows to deploy the project via git on pythonanywhere.

===== local deplopment =====

Steps to run on a local system:
1. Clone the repo
```
git clone https://gitlab.com/scratsid/yearbook.git
```
2. Install dependencies
```
cd yearbook
pip install -r requirements.txt
```
3. Create a db: make a copy of the file sample_db.json with the name db.json
```
cp db/sample.db.json db/db.json
```
4. To run it locally
```
python3 src/yearbook.py
```
5. Logging in
To log in the admin section use the credentials 'admin' & 'p@ss'

6. Database population.
The users db is populated via a csv file. The required fields for the csv are 'Roll' and 'Name'. If a predermined password needs to be populated please add the field 'password' to the csv file otherwise the longest part of the name will be used as the password(case sensitive). Any other fields of the csv will populated as is in the database.


===== Docker Deployment =====

1. Clone the repo
```
git clone https://gitlab.com/scratsid/yearbook.git
```

2. Build image from Dockerfile
```
cd yearbook
docker build -t pyweb/yearbook .
```

3. Run Docker
```
docker run --name yearbook -p <host-machine-port>:8080 pyweb/yearbook
```

4. The app will be available at http://localhost:<host-machine-port>
